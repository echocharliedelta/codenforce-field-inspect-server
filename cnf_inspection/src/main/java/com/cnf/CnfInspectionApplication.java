package com.cnf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CnfInspectionApplication {

  public static void main(String[] args) {
    SpringApplication.run(CnfInspectionApplication.class, args);
  }

}
