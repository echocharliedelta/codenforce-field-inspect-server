package com.cnf.config;

import com.cnf.interceptor.AuthPeriodTokenInterceptor;
import com.cnf.interceptor.UserLoginTokenInterceptor;
import javax.annotation.Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

  @Resource
  private AuthPeriodTokenInterceptor authPeriodTokenInterceptor;

  @Resource
  private UserLoginTokenInterceptor userLoginTokenInterceptor;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(authPeriodTokenInterceptor)
        .addPathPatterns("/occInspection/periodAuth/**").order(0);
    registry.addInterceptor(userLoginTokenInterceptor)
        .addPathPatterns("/occInspection/userAuth/*").order(1);
  }
}
