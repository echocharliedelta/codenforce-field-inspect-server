package com.cnf.controller.grpc;

import com.cnf.AuthorizationServiceGrpc.AuthorizationServiceBlockingStub;
import com.cnf.IsValidMunicipalityAuthPeriodRequest;
import com.cnf.IsValidUserRequest;
import java.net.ConnectException;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationServiceImpl {

  @GrpcClient("authorizationService")
  private AuthorizationServiceBlockingStub authorizationServiceBlockingStub;

  public boolean isValidLoginMunicipalityAuthPeriod(int municipalityAuthPeriodId, int userId,
      int municipalityCode) {
    IsValidMunicipalityAuthPeriodRequest request = IsValidMunicipalityAuthPeriodRequest
        .newBuilder()
        .setMunicipalityAuthPeriodId(municipalityAuthPeriodId)
        .setUserId(userId)
        .setMunicipalityCode(municipalityCode)
        .build();
    return authorizationServiceBlockingStub.isValidMunicipalityAuthPeriod(
        request).getIsValidAuthPeriod();
  }

  public int  isValidUser(String userLoginToken) {
    IsValidUserRequest isValidUserRequest = IsValidUserRequest
        .newBuilder()
        .setUserToken(userLoginToken)
        .build();
    return authorizationServiceBlockingStub.isValidUser(isValidUserRequest).getUserId();
  }
}
