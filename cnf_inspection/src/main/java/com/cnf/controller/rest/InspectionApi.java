package com.cnf.controller.rest;

import com.cnf.dto.BlobDTO;
import com.cnf.dto.OccInspectionDispatchDTO;
import com.cnf.exception.FetchingOccInspectionTaskException;
import com.cnf.exception.UploadInspectionFailException;
import com.cnf.service.IOccInspectionDispatchService;
import com.cnf.service.IOccInspectionInfraService;
import com.cnf.service.IOccInspectionSynchronizeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.time.OffsetDateTime;


@RestController
@RequestMapping("/occInspection")
public class InspectionApi {

  Logger logger = LoggerFactory.getLogger(InspectionApi.class);
  @Resource
  private IOccInspectionSynchronizeService inspectionService;

  @Resource
  private IOccInspectionDispatchService occInspectionHeavyService;

  @Resource
  private IOccInspectionInfraService occInspectionInfraService;


  @GetMapping("/test")
  public ResponseEntity<?> testLogin() {
    return new ResponseEntity<>("Inspection Service", HttpStatus.OK);
  }

  @GetMapping("/periodAuth/determinations")
  public ResponseEntity<?> getAllDeterminations(@RequestParam(required = false) String lastupdatedts) {
    return new ResponseEntity<>(occInspectionInfraService.getAllOccInspectionDeterminations(lastupdatedts),
            HttpStatus.OK);
  }

  @GetMapping("/periodAuth/causes")
  public ResponseEntity<?> getAllCauses(@RequestParam(required = false) String lastupdatedts) {
    return new ResponseEntity<>(occInspectionInfraService.getAllOccInspectionCauses(lastupdatedts),
            HttpStatus.OK);
  }

  @GetMapping("/periodAuth/intensityclasses")
  public ResponseEntity<?> getAllIntensityClasses(@RequestParam(required = false) String lastupdatedts) {
    return new ResponseEntity<>(occInspectionInfraService.getAllIntensityClasses(lastupdatedts),
            HttpStatus.OK);
  }

  @GetMapping("/periodAuth/checklists")
  public ResponseEntity<?> getAllChecklists(@RequestParam(required = false) String lastupdatedts) {
    return new ResponseEntity<>(occInspectionInfraService.getAllOccChecklist(lastupdatedts),
            HttpStatus.OK);
  }

  @GetMapping("/periodAuth/codeelementguides")
  public ResponseEntity<?> getAllCodeElementGuides(@RequestParam(required = false) String lastupdatedts) {
    return new ResponseEntity<>(occInspectionInfraService.getAllCodeElementGuides(lastupdatedts),
            HttpStatus.OK);
  }

  @GetMapping("/periodAuth/codeelements")
  public ResponseEntity<?> getAllCodeElements(@RequestParam(required = false) String lastupdatedts) {
    return new ResponseEntity<>(occInspectionInfraService.getAllCodeElements(lastupdatedts),
            HttpStatus.OK);
  }

  @GetMapping("/periodAuth/checklistspacetypes")
  public ResponseEntity<?> getAllOccChecklistSpaceTypes(@RequestParam(required = false) String lastupdatedts) {
    return new ResponseEntity<>(occInspectionInfraService.getAllOccChecklistSpaceTypes(lastupdatedts),
            HttpStatus.OK);
  }

  @GetMapping("/periodAuth/checklistspacetypeelements")
  public ResponseEntity<?> getAllOccChecklistSpaceTypeElements(@RequestParam(required = false) String lastupdatedts) {
    return new ResponseEntity<>(occInspectionInfraService.getAllOccChecklistSpaceTypeElements(lastupdatedts),
            HttpStatus.OK);
  }

  @GetMapping("/periodAuth/dispatch/all")
  public ResponseEntity<?> pullOccInspectionDispatchAll(@RequestParam(required = false) String lastupdatedts) {
    try {
      return new ResponseEntity<>(occInspectionHeavyService.getOccInspectionDispatchList(lastupdatedts),
              HttpStatus.OK);
    } catch (FetchingOccInspectionTaskException e) {
      logger.error(String.format("Date: %s, %s", OffsetDateTime.now(), e));
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

  @PostMapping("/periodAuth/uploadInspection")
  public ResponseEntity<?> uploadInspection(
          @RequestBody OccInspectionDispatchDTO occInspectionDispatchDTO) {
    try {
      OccInspectionDispatchDTO uploadedInspectionDTO = inspectionService.uploadInspection(occInspectionDispatchDTO);
      return new ResponseEntity<>(uploadedInspectionDTO, HttpStatus.OK);
    } catch (UploadInspectionFailException e) {
      logger.error(String.format("Date: %s, %s", OffsetDateTime.now(), e));
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

  @PostMapping("/periodAuth/dispatch/uploadBlob")
  public ResponseEntity<String> uploadBlob(@Valid @RequestBody BlobDTO blobDTO) {
    int bytesId = inspectionService.uploadBlob(blobDTO);
    return ResponseEntity.ok("Blob uploaded successfully with bytesId " + bytesId);
  }

}
