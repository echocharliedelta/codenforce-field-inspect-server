package com.cnf.dao;

import com.cnf.domain.BlobBytes;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BlobBytesMapper {
  int insertBlobBytes(BlobBytes blobBytes);
}
