package com.cnf.dao;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BlobTypeMapper {
    Integer getBlobTypeId(String fileName);
}
