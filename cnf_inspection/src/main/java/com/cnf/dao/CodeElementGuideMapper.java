package com.cnf.dao;

import com.cnf.domain.CodeElementGuide;
import org.apache.ibatis.annotations.Mapper;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface CodeElementGuideMapper {
  List<CodeElementGuide> selectAllCodeElementGuides(int muniCode, Timestamp lastupdatedts);
}
