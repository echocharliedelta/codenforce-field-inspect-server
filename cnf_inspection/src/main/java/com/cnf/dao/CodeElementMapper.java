package com.cnf.dao;

import com.cnf.domain.CodeElement;
import org.apache.ibatis.annotations.Mapper;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface CodeElementMapper {

  List<CodeElement> selectAllCodeElements(int muniCode, Timestamp lastupdatedts);
}
