package com.cnf.dao;

import com.cnf.domain.CodeSetElement;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CodeSetElementMapper {
  CodeSetElement selectCodeSetElementById(int codeSetElementId);
}
