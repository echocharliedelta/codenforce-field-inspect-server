package com.cnf.dao;

import com.cnf.domain.CodeSource;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CodeSourceMapper {
  CodeSource selectCodeSourceById(int sourceId);
}
