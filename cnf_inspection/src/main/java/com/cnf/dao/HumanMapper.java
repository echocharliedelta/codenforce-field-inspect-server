package com.cnf.dao;

import com.cnf.domain.Human;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HumanMapper {

  Human selectHumanById(int humanId);
}
