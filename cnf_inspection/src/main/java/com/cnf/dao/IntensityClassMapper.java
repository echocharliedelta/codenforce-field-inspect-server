package com.cnf.dao;

import com.cnf.domain.IntensityClass;
import org.apache.ibatis.annotations.Mapper;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface IntensityClassMapper {

  List<IntensityClass> selectAllIntensityClasses(int municode, Timestamp lastupdatedts);
}
