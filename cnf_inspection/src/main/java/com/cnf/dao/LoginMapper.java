package com.cnf.dao;

import com.cnf.domain.Login;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LoginMapper {

  Login selectUserByUserId(int userId);
}
