package com.cnf.dao;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MunicipalityMapper {
  Integer getOccperiodByMunicode(int muniCode);
}
