package com.cnf.dao;

import com.cnf.domain.OccCheckList;
import org.apache.ibatis.annotations.Mapper;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface OccCheckListMapper {

  List<OccCheckList> selectAllChecklist(int municode, Timestamp lastupdatedts);
}
