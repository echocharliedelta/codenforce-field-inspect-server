package com.cnf.dao;

import com.cnf.domain.OccChecklistSpaceType;
import com.cnf.domain.OccChecklistSpaceTypeElement;

import java.sql.Timestamp;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OccChecklistSpaceTypeElementMapper {
  OccChecklistSpaceTypeElement selectOccSpaceTypeElementById(int spaceElementId);
  List<OccChecklistSpaceTypeElement> selectAllOccSpaceTypeElementsById(int muniCode, Timestamp lastupdatedts);

}
