package com.cnf.dao;

import com.cnf.domain.OccChecklistSpaceType;
import org.apache.ibatis.annotations.Mapper;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface OccChecklistSpaceTypeMapper {

  List<OccChecklistSpaceType> selectALlChecklistSpaceType(int muniCode, Timestamp lastupdatedts);
}
