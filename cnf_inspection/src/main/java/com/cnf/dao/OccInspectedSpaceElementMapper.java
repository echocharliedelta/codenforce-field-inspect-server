package com.cnf.dao;

import com.cnf.domain.OccInspectedSpaceElement;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OccInspectedSpaceElementMapper {

  List<OccInspectedSpaceElement> selectOccInspectedSpaceElementBySpaceId(
          int inspectedSpaceId);

  int insertInspectedSpaceElement(OccInspectedSpaceElement occInspectedSpaceElement);

  OccInspectedSpaceElement selectByOccInspectedSpaceElementId(int occInspectedSpaceElementId);

  void updateOccInspectedSpaceElement(OccInspectedSpaceElement occInspectedSpaceElement);

  boolean checkOccInspectedSpaceElementExists(int occInspectedSpaceElementId);

  boolean checkInspectionAndElementAssociation(int inspectionId, int inspectedSpaceElementId);
}
