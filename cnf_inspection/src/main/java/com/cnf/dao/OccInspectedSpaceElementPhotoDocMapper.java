package com.cnf.dao;

import com.cnf.domain.OccInspectedSpaceElementPhotoDoc;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OccInspectedSpaceElementPhotoDocMapper {

  int insertOccInspectedSpaceElementPhotoDoc(
      OccInspectedSpaceElementPhotoDoc occInspectedSpaceElementPhotoDoc);
}
