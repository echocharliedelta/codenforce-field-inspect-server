package com.cnf.dao;

import com.cnf.domain.OccInspectedSpace;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OccInspectedSpaceMapper {

  List<OccInspectedSpace> selectOccInspectedSpaceByOccInspectionId(int occInspectionId);

  int insertInspectedSpace(OccInspectedSpace occInspectedSpace);

  OccInspectedSpace selectByOccInspectedSpaceId(int occInspectedSpaceId);

  void updateInspectedSpace(OccInspectedSpace occInspectedSpace);

  boolean checkInspectionAndInspectedSpaceAssociation(int inspectionId, int inspectedSpaceId);

}
