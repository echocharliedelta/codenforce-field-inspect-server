package com.cnf.dao;

import com.cnf.domain.OccInspectionCause;
import org.apache.ibatis.annotations.Mapper;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface OccInspectionCauseMapper {
    List<OccInspectionCause> selectAllCauses(int municode, Timestamp lastupdatedts);
}
