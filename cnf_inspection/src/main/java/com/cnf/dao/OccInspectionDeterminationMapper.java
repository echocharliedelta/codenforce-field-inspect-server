package com.cnf.dao;

import com.cnf.domain.OccInspectionDetermination;
import org.apache.ibatis.annotations.Mapper;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface OccInspectionDeterminationMapper {
    List<OccInspectionDetermination> selectAllDeterminations(int municode, Timestamp lastupdatedts);
}
