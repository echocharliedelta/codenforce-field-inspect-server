package com.cnf.dao;

import com.cnf.domain.OccInspectionDispatch;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

@Mapper
public interface OccInspectionDispatchMapper {

  int insertInspectionDispatch(OccInspectionDispatch occInspectionDispatch,int userId);

  int updateSyncNotes(int dispatchId, String logs);

  List<OccInspectionDispatch> selectUnSynchronizeInspectionDispatch(int userId, int muniCode, Timestamp lastupdatedts);

  int updateOccInspectionDispatchAfterRetrieve(@Param("userId") int userId, @Param("dispatchId") int dispatchId);

  int updateOccInspectionDispatchAfterSyn(@Param("userId") int userId, @Param("dispatchId") int dispatchId);

  boolean checkDispatchInspectionExists(int dispatchId);
}
