package com.cnf.dao;

import com.cnf.domain.OccInspection;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OccInspectionMapper {

  int updateInspectionById(OccInspection occInspection, int userId);
  OccInspection selectOccInspectionByInspectionId(int inspectionId);
  int insertInspection(OccInspection occInspection, String propertyAddress, String unitNumber);
  boolean checkInspectionExists(int inspectionId);

}
