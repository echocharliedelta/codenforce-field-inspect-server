package com.cnf.dao;

import com.cnf.domain.OccInspectionPhotoDoc;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OccInspectionPhotoDocMapper {
    int insertOccInspectionPhotoDoc(
            OccInspectionPhotoDoc occInspectionPhotoDoc);
}
