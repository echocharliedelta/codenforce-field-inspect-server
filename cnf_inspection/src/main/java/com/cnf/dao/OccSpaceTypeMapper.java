package com.cnf.dao;

import com.cnf.domain.OccSpaceType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OccSpaceTypeMapper {

  OccSpaceType selectSpaceTypeById(int spaceTypeId);
}
