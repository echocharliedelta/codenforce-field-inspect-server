package com.cnf.dao;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ParcelMapper {
  String getAddressByParcelKey(int parcelId);
  Integer getParcelKeyByInspectionId(Integer inspectionId);
}
