package com.cnf.dao;

import com.cnf.domain.ParcelUnit;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ParcelUnitMapper {
    ParcelUnit selectParcelByPeriodId(int periodId);
    ParcelUnit selectParcelByCaseId(int caseId);
}
