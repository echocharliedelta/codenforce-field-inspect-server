package com.cnf.dao;

import com.cnf.domain.PhotoDoc;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PhotoDocMapper {
  int insertPhotoDoc(PhotoDoc photoDoc);

}
