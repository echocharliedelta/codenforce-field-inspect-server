package com.cnf.domain;

import java.time.OffsetDateTime;
import lombok.Data;

@Data
public class BlobBytes {

  private Integer bytesId;
  private OffsetDateTime createdTS;
  private byte[] blob;
  private Integer uploadedByUserId;
  private String fileName;

}
