package com.cnf.domain;

import java.time.OffsetDateTime;
import lombok.Data;

@Data
public class CodeElement {

  private Integer elementId;
  private Integer codeSourceId;
  private Integer ordChapterNo;
  private String ordChapterTitle;
  private String ordSecNum;
  private String ordSecTitle;
  private String ordSubSecNum;
  private String ordSubSecTitle;
  private String ordTechnicalText;
  private Integer guideEntryId;
  private String notes;
  private OffsetDateTime lastUpdatedTS;
  private OffsetDateTime deactivatedTS;
  private String headerStringStatic;

}
