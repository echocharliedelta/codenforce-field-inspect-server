package com.cnf.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class CodeElementGuide {

  private Integer guideEntryId;
  private String category;
  private String subCategory;
  private String description;
  private String enforcementGuidelines;
  private String inspectionGuidelines;
  private Boolean priority;
  private OffsetDateTime lastupdatedTs;
  private OffsetDateTime deactivatedTs;

}
