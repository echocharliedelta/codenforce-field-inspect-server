package com.cnf.domain;

import java.time.OffsetDateTime;
import lombok.Data;

@Data
public class CodeSetElement {

  private Integer codeSetElementId;
  private Integer codeSetId;
  private Integer codeElementId;
  private Double elementMaxPenalty;
  private Double elementMinPenalty;
  private Double elementNormPenalty;
  private String penaltyNotes;
  private Integer normDaysToComply;
  private String daysToComplyNotes;
  private String muniSpecificNotes;
  private Integer defaultSeverityClassId;
  private Integer feeId;
  private String defaultViolationDescription;
  private OffsetDateTime createdTS;
  private Integer createdByUserId;
  private OffsetDateTime lastUpdatedTS;
  private Integer lastUpdatedByUserId;
  private OffsetDateTime deActivatedTS;
  private Integer deActivatedByUserid;
}
