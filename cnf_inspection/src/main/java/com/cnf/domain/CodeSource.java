package com.cnf.domain;

import lombok.Data;

@Data
public class CodeSource {

  private Integer sourceId;
  private String name;
  private Integer year;
  private String description;
  private Boolean deactivatedTS;
  private String url;
  private String notes;

}
