package com.cnf.domain;

import lombok.Data;

@Data
public class Human {

  private Integer humanId;
  private String name;
  private Boolean under18;
  private String jobTitle;
}
