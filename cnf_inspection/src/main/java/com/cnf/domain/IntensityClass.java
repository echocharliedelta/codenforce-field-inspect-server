package com.cnf.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class IntensityClass {

  private Integer classId;
  private String title;
  private Integer municode;
  private Integer numericRating;
  private String schemaName;
  private Boolean active;
  private Integer iconId;
  private OffsetDateTime lastupdatedTs;

}
