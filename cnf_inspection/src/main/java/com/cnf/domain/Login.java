package com.cnf.domain;

import lombok.Data;

@Data
public class Login {

  private Integer userId;
  private Integer humanId;

}
