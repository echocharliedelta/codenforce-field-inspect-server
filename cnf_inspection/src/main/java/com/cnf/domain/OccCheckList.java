package com.cnf.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccCheckList {

  private Integer checkListId;
  private String title;
  private String description;
  private Integer municode;
  private Integer governingCodeSourceId;
  private OffsetDateTime createdTS;
  private OffsetDateTime lastupdatedTs;
  private OffsetDateTime deactivatedTs;

}
