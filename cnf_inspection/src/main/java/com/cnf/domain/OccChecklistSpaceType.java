package com.cnf.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccChecklistSpaceType {

  private Integer checklistSpaceTypeId;
  private Integer checklistId;
  private Boolean required;
  private Integer spaceTypeId;
  private String notes;
  private OffsetDateTime deactivatedTs;
  private OffsetDateTime lastupdatedTs;

}
