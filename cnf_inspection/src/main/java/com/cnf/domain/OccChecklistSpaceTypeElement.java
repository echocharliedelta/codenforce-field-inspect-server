package com.cnf.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccChecklistSpaceTypeElement {

  private Integer spaceElementId;
  private Boolean required;
  private Integer checklistSpaceTypeId;
  private Integer codeSetElementId;
  private String notes;
  private OffsetDateTime deactivatedTs;
  private OffsetDateTime lastupdatedTs;
}