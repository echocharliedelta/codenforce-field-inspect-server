package com.cnf.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccInspectedSpace {

  private Integer inspectedSpaceId;
  private Integer occInspectionId;
  private Integer occLocationDescriptionId;
  private Integer addedToChecklistByUserid;
  private OffsetDateTime addedToChecklistTS;
  private Integer occChecklistSpaceTypeId;
  private Integer inspectedSpaceDeactivatedTS;
  private OffsetDateTime inspectedSpaceDeactivatedByUserId;

}
