package com.cnf.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccInspectedSpaceElement {

  private Integer inspectedSpaceElementId;
  private String notes;
  private Integer locationDescriptionId;
  private Integer lastInspectedByUserId;
  private OffsetDateTime lastInspectedTS;
  private Integer complianceGrantedByUserId;
  private OffsetDateTime complianceGrantedTS;
  private Integer inspectedSpaceId;
  private Integer overrideRequiredFlagNotInspectedUserid;
  private Integer occChecklistSpaceTypeElementId;
  private Integer failureSeverityIntensityClassId;
  private Boolean migrateToCECaseOnFail;
  private OffsetDateTime transferredTS;
  private Integer transferredByUserId;
  private Integer transferredToCECaseId;

}
