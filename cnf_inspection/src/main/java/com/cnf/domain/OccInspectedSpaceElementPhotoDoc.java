package com.cnf.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OccInspectedSpaceElementPhotoDoc {
  private Integer photoDocId;
  private Integer inspectedSpaceElementId;
}
