package com.cnf.domain;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import lombok.Data;

@Data
public class OccInspection {

  private Integer inspectionId;
  private Integer occPeriodId;
  private Integer inspectorUserId;
  private Integer publicAccessCC;
  private boolean enablePACC;
  private String notesPreInspection;
  private Integer thirdPartyInspectorPersonId;
  private OffsetDateTime thirdPartyInspectorApprovalTS;
  private Integer thirdPartyInspectorApprovalBy;
  private Integer maxOccupantsAllowed;
  private Integer numBedRooms;
  private Integer numBathRooms;
  private Integer occChecklistId;
  private OffsetDateTime effectiveDate;
  private OffsetDateTime createdTS;
  private Integer followUpToInspectionId;
  private OffsetDateTime timeStart;
  private OffsetDateTime timeEnd;
  private Integer determinationDetId;
  private Integer determinationByUserId;
  private OffsetDateTime determinationTS;
  private String remarks;
  private String generalComments;
  private OffsetDateTime deactivatedTS;
  private Integer deactivatedByUserId;
  private Integer createdByUserId;
  private OffsetDateTime lastUpdatedTS;
  private Integer lastUpdatedByUserId;
  private Integer causeId;
  private Integer ceCaseId;
  private Boolean isFinished;

}
