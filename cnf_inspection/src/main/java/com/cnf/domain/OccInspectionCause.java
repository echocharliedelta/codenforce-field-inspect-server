package com.cnf.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccInspectionCause {
    private Integer causeId;
    private String title;
    private OffsetDateTime lastupdatedTs;
    private OffsetDateTime deactivatedTs;
    private Integer muniCode;
}
