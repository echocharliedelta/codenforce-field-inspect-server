package com.cnf.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccInspectionDetermination {
    private Integer determinationId;
    private String title;
    private String description;
    private Boolean qualifiesAsPassed;
    private OffsetDateTime lastupdatedTs;
    private OffsetDateTime deactivatedTs;
    private Integer muniCode;
}
