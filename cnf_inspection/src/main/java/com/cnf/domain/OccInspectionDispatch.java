package com.cnf.domain;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccInspectionDispatch {

  private Integer dispatchId;
  private Integer createdByUserId;
  private OffsetDateTime creationTS;
  private String dispatchNotes;
  private Integer inspectionId;
  private OffsetDateTime retrievalTS;
  private Integer retrievedByUserId;
  private OffsetDateTime synchronizationTS;
  private String synchronizationNotes;
  private Integer muniCode;
  private OffsetDateTime dispatchLastUpdatedTs;
}
