package com.cnf.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OccInspectionPhotoDoc {
    private Integer photoDocId;
    private Integer inspectionId;
}
