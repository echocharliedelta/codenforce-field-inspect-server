package com.cnf.domain;

import lombok.Data;

@Data
public class OccSpaceType {

  private Integer spaceTypeId;
  private String spaceTitle;
  private String description;

}
