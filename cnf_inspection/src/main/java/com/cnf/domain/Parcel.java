package com.cnf.domain;

import java.time.OffsetDateTime;
import lombok.Data;

@Data
public class Parcel {

  private Integer parcelKey;
  private String parcelidcnty;
  private Integer sourceId;
  private OffsetDateTime createdTS;
  private Integer createdByUserid;
  private OffsetDateTime lastUpdatedTS;
  private Integer lastUpdatedByUserId;
  private OffsetDateTime deActivatedTS;
  private Integer deActivatedByUserId;
  private String notes;
  private Integer municode;
  private String lotAndBlock;
  private Integer broadviewPhotoDocId;

}
