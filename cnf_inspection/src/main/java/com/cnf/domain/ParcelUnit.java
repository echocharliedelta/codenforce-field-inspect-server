package com.cnf.domain;

import lombok.Data;

@Data
public class ParcelUnit {
    private Integer unitId;
    private String unitNumber;
    private Integer parcelKey;
}
