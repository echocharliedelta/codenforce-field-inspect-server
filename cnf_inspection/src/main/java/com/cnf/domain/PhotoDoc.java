package com.cnf.domain;

import java.time.OffsetDateTime;
import lombok.Data;

@Data
public class PhotoDoc {

  private Integer photoDocId;
  private String photoDocDescription;
  private boolean photoDocCommitted;
  private Integer blobBytesId;
  private Integer municode;
  private Integer blobTypeId;
  private byte[] metaDataMap;
  private String title;
  private OffsetDateTime createdTS;
  private Integer createdByUserid;
  private OffsetDateTime lastUpdatedTS;
  private Integer lastUpdatedByUserId;
  private OffsetDateTime deActivatedTS;
  private Integer deActivatedByUserUd;
  private OffsetDateTime dateOfRecord;
  private Boolean courtDocument;
}
