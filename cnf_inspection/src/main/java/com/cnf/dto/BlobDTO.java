package com.cnf.dto;

import com.cnf.domain.BlobBytes;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class BlobDTO {
    @NotEmpty(message = "Inspection id cannot be empty")
    private Integer inspectionId;
    @NotEmpty(message = "inspected space element id cannot be empty")
    private List<Integer> inspectedSpaceElementIds;
    private BlobBytes blobBytes;

}
