package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class CodeElementDTO {
    private Integer elementId;
    private Integer codeSourceId;
    private Integer ordChapterNo;
    private String ordChapterTitle;
    private String ordSecNum;
    private String ordSecTitle;
    private String ordSubSecNum;
    private String ordSubSecTitle;
    private String ordTechnicalText;
    private Integer guideEntryId;
    private String notes;
    private String ordSubSubSecNum;
    private OffsetDateTime lastUpdatedTS;
    private OffsetDateTime deactivatedTS;
    private String headerStringStatic;
    private String name;
    private Integer year;
}
