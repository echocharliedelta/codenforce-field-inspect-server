package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class CodeElementGuideDTO {
    private Integer guideEntryId;
    private String category;
    private String description;
    private OffsetDateTime lastupdatedTs;
    private OffsetDateTime deactivatedTs;
}
