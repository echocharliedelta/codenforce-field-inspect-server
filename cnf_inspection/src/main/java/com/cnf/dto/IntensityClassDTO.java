package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class IntensityClassDTO {
    private Integer classId;
    private String title;
    private Integer municode;
    private Boolean active;
    private OffsetDateTime lastupdatedTs;
}
