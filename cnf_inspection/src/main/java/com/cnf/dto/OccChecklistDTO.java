package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccChecklistDTO {
    private Integer checkListId;
    private String title;
    private String description;
    private Integer municode;
    private Integer governingCodeSourceId;
    private OffsetDateTime lastupdatedTs;
    private OffsetDateTime deactivatedTs;
}
