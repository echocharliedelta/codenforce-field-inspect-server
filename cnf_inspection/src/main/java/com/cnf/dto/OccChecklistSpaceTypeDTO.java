package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccChecklistSpaceTypeDTO {
    private Integer checklistSpaceTypeId;
    private Integer checklistId;
    private Boolean required;
    private OffsetDateTime deactivatedTs;
    private OffsetDateTime lastupdatedTs;
    private Integer spaceTypeId;
    private String spaceTitle;
    private String description;
}
