package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccChecklistSpaceTypeElementDTO {
    private Integer spaceElementId;
    private Boolean required;
    private Integer checklistSpaceTypeId;
    private Integer codeSetElementId;
    private OffsetDateTime deactivatedTs;
    private OffsetDateTime lastupdatedTs;
    private Integer codeElementLinkedId;
}
