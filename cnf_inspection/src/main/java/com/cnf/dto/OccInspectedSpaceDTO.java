package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;
import java.util.List;

@Data
public class OccInspectedSpaceDTO {
    private Integer inspectedSpaceId;
    private String appInspectedSpaceId;
    private Integer occInspectionId;
    private Integer addedToChecklistByUserid;
    private OffsetDateTime addedToChecklistTS;
    private Integer occChecklistSpaceTypeId;
    private Integer inspectedSpaceDeactivatedTS;
    private OffsetDateTime inspectedSpaceDeactivatedByUserId;
    private List<OccInspectedSpaceElementDTO> occInspectedSpaceElementDTOList;

}
