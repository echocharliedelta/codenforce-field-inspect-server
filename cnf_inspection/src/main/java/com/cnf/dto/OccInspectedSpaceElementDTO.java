package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccInspectedSpaceElementDTO {
    private Integer inspectedSpaceElementId;
    private String appInspectedSpaceElementId;
    private String notes;
    private Integer lastInspectedByUserId;
    private OffsetDateTime lastInspectedTS;
    private Integer complianceGrantedByUserId;
    private OffsetDateTime complianceGrantedTS;
    private Integer inspectedSpaceId;
    private Integer occChecklistSpaceTypeElementId;
    private Integer failureSeverityIntensityClassId;
    private Integer codeSetElementId;
    private Integer codeElementId;
    private Boolean required;
}
