package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccInspectionCauseDTO {
    private Integer causeId;
    private String title;
    private OffsetDateTime lastupdatedTs;
    private OffsetDateTime deactivatedTs;
    private Integer muniCode;
}
