package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class OccInspectionDeterminationDTO {
    private Integer determinationId;
    private String title;
    private String description;
    private Boolean qualifiesAsPassed;
    private OffsetDateTime lastupdatedTs;
    private OffsetDateTime deactivatedTs;
    private Integer muniCode;
}
