package com.cnf.dto;

import lombok.Data;

import java.time.OffsetDateTime;
import java.util.List;

@Data
public class OccInspectionDispatchDTO {
    private Integer inspectionId;
    private Integer dispatchId;
    private String appInspectionId;
    private Integer muniCode;
    private String propertyAddress;
    private String unitNumber;
    private Integer inspectorUserId;
    private String InspectorName;
    private Integer occChecklistId;
    private String notesPreInspection;
    private Integer maxOccupantsAllowed;
    private Integer numBedRooms;
    private Integer numBathRooms;
    private OffsetDateTime effectiveDate;
    private OffsetDateTime timeStart;
    private OffsetDateTime timeEnd;
    private Integer determinationDetId;
    private Integer determinationByUserId;
    private OffsetDateTime determinationTS;
    private String generalComments;
    private Integer causeId;
    private String dispatchNotes;
    private OffsetDateTime retrievalTS;
    private OffsetDateTime synchronizationTS;
    private String synchronizationNotes;
    private OffsetDateTime dispatchLastUpdatedTs;
    private List<OccInspectedSpaceDTO> occInspectedSpaceDTOList;
}
