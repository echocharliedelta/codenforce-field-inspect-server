package com.cnf.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDTO {

  Integer userid;
  Integer municode;
  Integer municipalityAuthPeriodId;
}
