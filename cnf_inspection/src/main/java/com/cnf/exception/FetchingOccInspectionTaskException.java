package com.cnf.exception;

public class FetchingOccInspectionTaskException extends Exception {

  public FetchingOccInspectionTaskException(String message) {
    super(message);
  }
}
