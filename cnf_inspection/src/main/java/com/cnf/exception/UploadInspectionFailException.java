package com.cnf.exception;

public class UploadInspectionFailException extends Exception {
  public UploadInspectionFailException(String message) {
    super(message);
  }
}
