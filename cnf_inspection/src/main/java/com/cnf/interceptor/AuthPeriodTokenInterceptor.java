package com.cnf.interceptor;

import com.cnf.controller.grpc.AuthorizationServiceImpl;
import com.cnf.dto.UserDTO;
import com.cnf.util.UserHolder;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;

@Service
public class AuthPeriodTokenInterceptor implements HandlerInterceptor {

  private static final String AUTHORIZATION_HEADER = "authorization";
  private static final String MUNICIPALITY_CODE_PARAM = "municipality_code";
  private static final String AUTH_PERIOD_PARAM = "auth_period";
  @Resource
  private AuthorizationServiceImpl authorizationService;

  Logger logger = LoggerFactory.getLogger(AuthPeriodTokenInterceptor.class);

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
      Object handler) {

    String userLoginToken = request.getHeader(AUTHORIZATION_HEADER);
    int userId = authorizationService.isValidUser(userLoginToken);
    if (userId == -1) {
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      return false;
    }

    try {
      Integer municipalityCode = Integer.parseInt(request.getParameter(MUNICIPALITY_CODE_PARAM));
      Integer municipalityAuthPeriodId = Integer.parseInt(request.getParameter(AUTH_PERIOD_PARAM));
      boolean isValidLoginMunicipalityAuthPeriod = authorizationService.isValidLoginMunicipalityAuthPeriod(
          municipalityAuthPeriodId, userId, municipalityCode);
      if (isValidLoginMunicipalityAuthPeriod) {
        UserDTO userDTO = new UserDTO(userId, municipalityCode, municipalityAuthPeriodId);
        UserHolder.saveUser(userDTO);
        return true;
      }
    } catch (Exception e) {
      logger.error(e.toString());
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }
    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    return false;
  }
}
