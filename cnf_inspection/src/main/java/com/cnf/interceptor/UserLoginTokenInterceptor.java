package com.cnf.interceptor;

import com.cnf.controller.grpc.AuthorizationServiceImpl;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;

@Service
public class UserLoginTokenInterceptor implements HandlerInterceptor {

  private static final String AUTHORIZATION_HEADER = "authorization";
  @Resource
  private AuthorizationServiceImpl authorizationService;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
      Object handler) {
    String userLoginToken = request.getHeader(AUTHORIZATION_HEADER);
    int userId = authorizationService.isValidUser(userLoginToken);
    if (userId == -1) {
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      return false;
    }
    return true;
  }
}
