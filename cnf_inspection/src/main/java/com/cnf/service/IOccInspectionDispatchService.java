package com.cnf.service;

import com.cnf.dto.OccInspectionDispatchDTO;
import com.cnf.exception.FetchingOccInspectionTaskException;

import java.util.List;

public interface IOccInspectionDispatchService {
  List<OccInspectionDispatchDTO> getOccInspectionDispatchList(String lastupdatedts) throws FetchingOccInspectionTaskException;
}
