package com.cnf.service;

import com.cnf.dto.CodeElementDTO;
import com.cnf.dto.CodeElementGuideDTO;
import com.cnf.dto.IntensityClassDTO;
import com.cnf.dto.OccChecklistDTO;
import com.cnf.dto.OccChecklistSpaceTypeDTO;
import com.cnf.dto.OccChecklistSpaceTypeElementDTO;
import com.cnf.dto.OccInspectionCauseDTO;
import com.cnf.dto.OccInspectionDeterminationDTO;

import java.util.List;

public interface IOccInspectionInfraService {
  List<OccInspectionDeterminationDTO> getAllOccInspectionDeterminations(String lastupdatedts);
  List<OccInspectionCauseDTO> getAllOccInspectionCauses(String lastupdatedts);
  List<IntensityClassDTO> getAllIntensityClasses(String lastupdatedts);
  List<OccChecklistDTO> getAllOccChecklist(String lastupdatedts);
  List<CodeElementGuideDTO> getAllCodeElementGuides(String lastupdatedts);
  List<CodeElementDTO> getAllCodeElements(String lastupdatedts);
  List<OccChecklistSpaceTypeDTO> getAllOccChecklistSpaceTypes(String lastupdatedts);
  List<OccChecklistSpaceTypeElementDTO> getAllOccChecklistSpaceTypeElements(String lastupdatedts);

}
