package com.cnf.service;

import com.cnf.dto.BlobDTO;
import com.cnf.dto.OccInspectionDispatchDTO;
import com.cnf.exception.UploadInspectionFailException;

public interface IOccInspectionSynchronizeService {
  int uploadBlob(BlobDTO blobDTO);
  OccInspectionDispatchDTO uploadInspection(OccInspectionDispatchDTO occInspectionDispatchDTO) throws UploadInspectionFailException;

}
