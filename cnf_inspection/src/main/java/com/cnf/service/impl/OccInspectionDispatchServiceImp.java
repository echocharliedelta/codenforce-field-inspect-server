package com.cnf.service.impl;

import com.cnf.dao.*;
import com.cnf.domain.*;
import com.cnf.dto.*;
import com.cnf.exception.FetchingOccInspectionTaskException;
import com.cnf.service.IOccInspectionDispatchService;
import com.cnf.util.UserHolder;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.cnf.util.LastUpdatedTimestampParser.parseLastUpdatedToTimestamp;

@Service
public class OccInspectionDispatchServiceImp implements IOccInspectionDispatchService {

  Logger logger = LoggerFactory.getLogger(OccInspectionDispatchServiceImp.class);

  @Resource
  private OccInspectionMapper occInspectionMapper;

  @Resource
  private LoginMapper loginMapper;

  @Resource
  private HumanMapper humanMapper;

  @Resource
  private OccInspectionDispatchMapper occInspectionDispatchMapper;

  @Resource
  private OccInspectedSpaceMapper occInspectedSpaceMapper;

  @Resource
  private OccInspectedSpaceElementMapper occInspectedSpaceElementMapper;

  @Resource
  private ParcelMapper parcelMapper;

  @Resource
  private OccChecklistSpaceTypeElementMapper occChecklistSpaceTypeElementMapper;

  @Resource
  private CodeSetElementMapper codeSetElementMapper;

  @Resource
  private ParcelUnitMapper parcelUnitMapper;

  @Autowired
  private ModelMapper modelMapper;

  @Transactional
  @Override
  public List<OccInspectionDispatchDTO> getOccInspectionDispatchList(String lastupdatedts) throws FetchingOccInspectionTaskException {
    UserDTO user = UserHolder.getUser();
    Timestamp lastupdated = parseLastUpdatedToTimestamp(lastupdatedts);
    logger.info(String.format("Date: %s, %s", OffsetDateTime.now(), user));

    try {
      List<OccInspectionDispatch> occInspectionDispatchList = occInspectionDispatchMapper.selectUnSynchronizeInspectionDispatch(user.getUserid(), user.getMunicode(), lastupdated);
        return occInspectionDispatchList.stream()
              .map(occInspectionDispatch -> {
                occInspectionDispatchMapper.updateOccInspectionDispatchAfterRetrieve(user.getUserid(),
                        occInspectionDispatch.getDispatchId());
                OccInspectionDispatchDTO occInspectionDispatchDTO = modelMapper.map(occInspectionDispatch, OccInspectionDispatchDTO.class);
                populateOccInspectionData(occInspectionDispatchDTO);
                return occInspectionDispatchDTO;
              })
              .collect(Collectors.toList());
    } catch (Exception e) {
      logger.error(String.format("Date: %s, %s", OffsetDateTime.now(), e));
      throw new FetchingOccInspectionTaskException("FetchingOccInspectionTaskException");
    }
  }

  private void populateOccInspectionData(OccInspectionDispatchDTO occInspectionDispatchDTO) {
    OccInspection occInspection = occInspectionMapper.selectOccInspectionByInspectionId(occInspectionDispatchDTO.getInspectionId());

    if (Objects.isNull(occInspection)) {
      return;
    }
    modelMapper.map(occInspection, occInspectionDispatchDTO);

    Integer occPeriodId = occInspection.getOccPeriodId();
    if (Objects.nonNull(occPeriodId)) {
      ParcelUnit parcelUnit = parcelUnitMapper.selectParcelByPeriodId(occPeriodId);
      if (Objects.nonNull(parcelUnit)) {
        handleParcelUnit(occInspectionDispatchDTO, parcelUnit);
      }
    }

    Integer ceCaseId = occInspection.getCeCaseId();
    if (Objects.nonNull(ceCaseId)) {
      ParcelUnit parcelUnit = parcelUnitMapper.selectParcelByCaseId(ceCaseId);
      if (Objects.nonNull(parcelUnit)) {
        handleParcelUnit(occInspectionDispatchDTO, parcelUnit);
      } else {
        Integer parcelKey = parcelMapper.getParcelKeyByInspectionId(occInspection.getInspectionId());
        if (Objects.nonNull(parcelKey)) {
          occInspectionDispatchDTO.setPropertyAddress(parcelMapper.getAddressByParcelKey(parcelKey));
        }
      }
    }
    occInspectionDispatchDTO.setInspectorName(getInspectorUserName(occInspection.getInspectorUserId()));

    List<OccInspectedSpaceDTO> occInspectedSpaceDTOList = mapToOccInspectedSpaces(occInspectedSpaceMapper.selectOccInspectedSpaceByOccInspectionId(occInspection.getInspectionId()));
    occInspectionDispatchDTO.setOccInspectedSpaceDTOList(occInspectedSpaceDTOList);
  }

  private void handleParcelUnit(OccInspectionDispatchDTO occInspectionDispatchDTO, ParcelUnit parcelUnit) {
    occInspectionDispatchDTO.setUnitNumber(parcelUnit.getUnitNumber());
    String parcelAddress = parcelMapper.getAddressByParcelKey(parcelUnit.getParcelKey());
    occInspectionDispatchDTO.setPropertyAddress(parcelAddress);
  }

  private List<OccInspectedSpaceDTO> mapToOccInspectedSpaces(List<OccInspectedSpace> occInspectedSpaceList) {
    return occInspectedSpaceList.stream()
            .map(this::mapToOccInspectedSpaceDTO)
            .collect(Collectors.toList());
  }

  private OccInspectedSpaceDTO mapToOccInspectedSpaceDTO(OccInspectedSpace occInspectedSpace) {
    OccInspectedSpaceDTO spaceDTO = modelMapper.map(occInspectedSpace, OccInspectedSpaceDTO.class);
    List<OccInspectedSpaceElementDTO> elementDTOList = mapToOccInspectedSpaceElements(occInspectedSpace);
    spaceDTO.setOccInspectedSpaceElementDTOList(elementDTOList);
    return spaceDTO;
  }

  private List<OccInspectedSpaceElementDTO> mapToOccInspectedSpaceElements(OccInspectedSpace occInspectedSpace) {
    return occInspectedSpaceElementMapper.selectOccInspectedSpaceElementBySpaceId(occInspectedSpace.getInspectedSpaceId())
            .stream()
            .map(this::mapToOccInspectedSpaceElementDTO)
            .collect(Collectors.toList());
  }

  private OccInspectedSpaceElementDTO mapToOccInspectedSpaceElementDTO(OccInspectedSpaceElement occInspectedSpaceElement) {
    OccInspectedSpaceElementDTO elementDTO = modelMapper.map(occInspectedSpaceElement, OccInspectedSpaceElementDTO.class);
    OccChecklistSpaceTypeElement checklistElement = occChecklistSpaceTypeElementMapper.selectOccSpaceTypeElementById(occInspectedSpaceElement.getOccChecklistSpaceTypeElementId());

    if (Objects.nonNull(checklistElement)) {
      elementDTO.setRequired(checklistElement.getRequired());
      elementDTO.setCodeSetElementId(checklistElement.getCodeSetElementId());

      if (Objects.nonNull(checklistElement.getCodeSetElementId()) && checklistElement.getCodeSetElementId() != 0) {
        CodeSetElement codeSetElement = codeSetElementMapper.selectCodeSetElementById(checklistElement.getCodeSetElementId());
        elementDTO.setCodeElementId(codeSetElement.getCodeElementId());
      }
    }
    return elementDTO;
  }

  public String getInspectorUserName(Integer inspectorUserId) {
    Login login = loginMapper.selectUserByUserId(inspectorUserId);
    if (Objects.isNull(login)) {
      return null;
    }

    Integer humanId = login.getHumanId();
    if (Objects.isNull(humanId)) {
      return null;
    }

    Human human = humanMapper.selectHumanById(humanId);
    if (Objects.isNull(human)) {
      return null;
    }
      return human.getName();
  }

}
