package com.cnf.service.impl;

import com.cnf.dao.*;
import com.cnf.domain.*;
import com.cnf.dto.*;
import com.cnf.service.IOccInspectionInfraService;
import com.cnf.util.UserHolder;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.cnf.util.LastUpdatedTimestampParser.parseLastUpdatedToTimestamp;

@Service
public class OccInspectionInfraServiceImp implements IOccInspectionInfraService {

    @Resource
    private CodeSourceMapper codeSourceMapper;

    @Resource
    private CodeElementMapper codeElementMapper;

    @Resource
    private OccChecklistSpaceTypeMapper occChecklistSpaceTypeMapper;

    @Resource
    private OccSpaceTypeMapper occSpaceTypeMapper;

    @Resource
    private CodeElementGuideMapper codeElementGuideMapper;

    @Resource
    private IntensityClassMapper intensityClassMapper;

    @Resource
    private OccCheckListMapper occCheckListMapper;

    @Resource
    private OccInspectionDeterminationMapper determinationMapper;

    @Resource
    private OccInspectionCauseMapper causeMapper;

    @Resource
    private OccChecklistSpaceTypeElementMapper occChecklistSpaceTypeElementMapper;

    @Resource
    private CodeSetElementMapper codeSetElementMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<OccInspectionDeterminationDTO> getAllOccInspectionDeterminations(String lastupdatedts) {
        UserDTO user = UserHolder.getUser();
        Timestamp lastupdated = parseLastUpdatedToTimestamp(lastupdatedts);
        List<OccInspectionDetermination> determinationsList =
                determinationMapper.selectAllDeterminations(user.getMunicode(), lastupdated);
        return determinationsList.stream().
                map(determination -> modelMapper.map(determination, OccInspectionDeterminationDTO.class)).
                collect(Collectors.toList());
    }

    @Override
    public List<OccInspectionCauseDTO> getAllOccInspectionCauses(String lastupdatedts) {
        UserDTO user = UserHolder.getUser();
        Timestamp lastupdated = parseLastUpdatedToTimestamp(lastupdatedts);
        List<OccInspectionCause> causesList =
                causeMapper.selectAllCauses(user.getMunicode(), lastupdated);
        return causesList.stream()
                .map(cause -> modelMapper.map(cause, OccInspectionCauseDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<IntensityClassDTO> getAllIntensityClasses(String lastupdatedts) {
        UserDTO user = UserHolder.getUser();
        Timestamp lastupdated = parseLastUpdatedToTimestamp(lastupdatedts);
        List<IntensityClass> intensityClassList = intensityClassMapper.selectAllIntensityClasses(user.getMunicode(), lastupdated);
        return intensityClassList.stream()
                .map(intensityClass -> modelMapper.map(intensityClass, IntensityClassDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<OccChecklistDTO> getAllOccChecklist(String lastupdatedts) {
        UserDTO user = UserHolder.getUser();
        Timestamp lastupdated = parseLastUpdatedToTimestamp(lastupdatedts);
        List<OccCheckList> occCheckListList = occCheckListMapper.selectAllChecklist(user.getMunicode(), lastupdated);
        return occCheckListList.stream()
                .map(occCheckList -> modelMapper.map(occCheckList, OccChecklistDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<CodeElementGuideDTO> getAllCodeElementGuides(String lastupdatedts) {
        UserDTO user = UserHolder.getUser();
        Timestamp lastupdated = parseLastUpdatedToTimestamp(lastupdatedts);
        List<CodeElementGuide> codeElementGuideList = codeElementGuideMapper.selectAllCodeElementGuides(user.getMunicode(), lastupdated);
        return codeElementGuideList.stream()
                .map(codeElementGuide -> modelMapper.map(codeElementGuide, CodeElementGuideDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<CodeElementDTO> getAllCodeElements(String lastupdatedts) {
        UserDTO user = UserHolder.getUser();
        Timestamp lastupdated = parseLastUpdatedToTimestamp(lastupdatedts);
        List<CodeElement> codeElementList = codeElementMapper.selectAllCodeElements(user.getMunicode(), lastupdated);
        return codeElementList.stream()
                .map(codeElement -> {
                    CodeElementDTO dto = modelMapper.map(codeElement, CodeElementDTO.class);
                    if (Objects.nonNull(dto.getCodeSourceId())) {
                        CodeSource source = codeSourceMapper.selectCodeSourceById(dto.getCodeSourceId());
                        if (Objects.nonNull(source)) {
                            dto.setName(source.getName());
                            dto.setYear(source.getYear());
                        }
                    }
                    return dto;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<OccChecklistSpaceTypeDTO> getAllOccChecklistSpaceTypes(String lastupdatedts) {
        UserDTO user = UserHolder.getUser();
        Timestamp lastupdated = parseLastUpdatedToTimestamp(lastupdatedts);
        List<OccChecklistSpaceType> occChecklistSpaceTypeList = occChecklistSpaceTypeMapper.selectALlChecklistSpaceType(user.getMunicode(), lastupdated);
        return occChecklistSpaceTypeList.stream()
                .map(occChecklistSpaceType -> {
                    OccChecklistSpaceTypeDTO dto = modelMapper.map(occChecklistSpaceType, OccChecklistSpaceTypeDTO.class);
                    if (Objects.nonNull(dto.getSpaceTypeId())) {
                        OccSpaceType spaceType = occSpaceTypeMapper.selectSpaceTypeById(dto.getSpaceTypeId());
                        if (Objects.nonNull(spaceType)) {
                            dto.setSpaceTitle(spaceType.getSpaceTitle());
                            dto.setDescription(spaceType.getDescription());
                        }
                    }
                    return dto;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<OccChecklistSpaceTypeElementDTO> getAllOccChecklistSpaceTypeElements(String lastupdatedts) {
        UserDTO user = UserHolder.getUser();
        Timestamp lastupdated = parseLastUpdatedToTimestamp(lastupdatedts);
        List<OccChecklistSpaceTypeElement> occChecklistSpaceTypeElementList = occChecklistSpaceTypeElementMapper.selectAllOccSpaceTypeElementsById(user.getMunicode(), lastupdated);
        return occChecklistSpaceTypeElementList.stream()
                .map(occChecklistSpaceTypeElement -> {
                    OccChecklistSpaceTypeElementDTO dto = modelMapper.map(occChecklistSpaceTypeElement, OccChecklistSpaceTypeElementDTO.class);
                    if (Objects.nonNull(dto.getCodeSetElementId())) {
                        CodeSetElement codeSetElement = codeSetElementMapper.selectCodeSetElementById(dto.getCodeSetElementId());
                        dto.setCodeElementLinkedId(codeSetElement.getCodeElementId());
                    }
                    return dto;
                })
                .collect(Collectors.toList());
    }

}
