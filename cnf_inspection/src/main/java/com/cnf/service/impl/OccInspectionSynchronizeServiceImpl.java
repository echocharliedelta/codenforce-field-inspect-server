package com.cnf.service.impl;

import com.cnf.dao.*;
import com.cnf.domain.*;
import com.cnf.dto.*;
import com.cnf.exception.ApiException;
import com.cnf.exception.ResourceNotFoundException;
import com.cnf.exception.UploadInspectionFailException;
import com.cnf.service.IOccInspectionSynchronizeService;
import com.cnf.util.UserHolder;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class OccInspectionSynchronizeServiceImpl implements IOccInspectionSynchronizeService {

  Logger logger = LoggerFactory.getLogger(OccInspectionSynchronizeServiceImpl.class);
  @Resource
  private OccInspectionMapper occInspectionMapper;

  @Resource
  private OccInspectedSpaceMapper occInspectedSpaceMapper;

  @Resource
  private OccInspectedSpaceElementMapper occInspectedSpaceElementMapper;

  @Resource
  private BlobBytesMapper blobBytesMapper;

  @Resource
  private PhotoDocMapper photoDocMapper;

  @Resource
  private OccInspectedSpaceElementPhotoDocMapper occInspectedSpaceElementPhotoDocMapper;

  @Resource
  private OccInspectionDispatchMapper occInspectionDispatchMapper;

  @Resource
  private OccInspectionPhotoDocMapper occInspectionPhotoDocMapper;

  @Resource
  private MunicipalityMapper municipalityMapper;

  @Resource
  private BlobTypeMapper blobTypeMapper;

  @Autowired
  private ModelMapper modelMapper;

  @Transactional
  @Override
  public int uploadBlob(BlobDTO blobDTO) {
    BlobBytes blobBytes = blobDTO.getBlobBytes();
    UserDTO user = UserHolder.getUser();
    int inspectionId = blobDTO.getInspectionId();
    List<Integer> inspectedSpaceElementIds = blobDTO.getInspectedSpaceElementIds();

    validateInspectionExists(inspectionId);

    blobBytesMapper.insertBlobBytes(blobBytes);
    int bytesId = blobBytes.getBytesId();

    PhotoDoc photoDoc = createPhotoDoc(bytesId, blobDTO, user);

    photoDocMapper.insertPhotoDoc(photoDoc);
    int photoDocId = photoDoc.getPhotoDocId();

    inspectedSpaceElementIds.stream()
            .filter(inspectedSpaceElementId -> {
              validateInspectedElementExists(inspectedSpaceElementId);
              validateInspectionAndElementAssociation(inspectionId, inspectedSpaceElementId);
              return true;
            })
            .forEach(inspectedSpaceElementId -> occInspectedSpaceElementPhotoDocMapper.insertOccInspectedSpaceElementPhotoDoc(new OccInspectedSpaceElementPhotoDoc(photoDocId, inspectedSpaceElementId)));

    occInspectionPhotoDocMapper.insertOccInspectionPhotoDoc(new OccInspectionPhotoDoc(photoDocId, blobDTO.getInspectionId()));
    return bytesId;
  }

  @Transactional
  @Override
  public OccInspectionDispatchDTO uploadInspection(OccInspectionDispatchDTO occInspectionDispatchDTO) throws UploadInspectionFailException {
    UserDTO user = UserHolder.getUser();
    StringBuilder synchronizationLog = new StringBuilder();
    try {
      OccInspection occInspection = modelMapper.map(occInspectionDispatchDTO, OccInspection.class);
      OccInspectionDispatch occInspectionDispatch = modelMapper.map(occInspectionDispatchDTO, OccInspectionDispatch.class);
      Integer inspectionId = occInspection.getInspectionId();
      if (!occInspectionMapper.checkInspectionExists(inspectionId)) {
        occInspection = createNewInspection(occInspection, occInspectionDispatchDTO.getPropertyAddress(), occInspectionDispatchDTO.getUnitNumber(), user);
        inspectionId = occInspection.getInspectionId();
        occInspectionDispatch = createNewOccInspectionDispatch(occInspectionDispatch, user.getUserid(), inspectionId);

        occInspectionDispatchDTO.setInspectionId(inspectionId);
        occInspectionDispatchDTO.setDispatchId(occInspectionDispatch.getDispatchId());
      } else {
        occInspectionDispatchMapper.updateOccInspectionDispatchAfterSyn(user.getUserid(), occInspectionDispatch.getDispatchId());
        occInspectionMapper.updateInspectionById(occInspection, user.getUserid());
      }

      List<OccInspectedSpaceDTO> occInspectedSpaceDTOList = occInspectionDispatchDTO.getOccInspectedSpaceDTOList();

      Integer finalInspectionId = inspectionId;
      if (Objects.nonNull(occInspectedSpaceDTOList)) {
        occInspectedSpaceDTOList.forEach(occInspectedSpaceDTO -> {
          OccInspectedSpace occInspectedSpace = modelMapper.map(occInspectedSpaceDTO, OccInspectedSpace.class);
          Integer inspectedSpaceId = updateInspectedSpaces(occInspectedSpace, finalInspectionId);
          occInspectedSpaceDTO.setInspectedSpaceId(inspectedSpaceId);

          Optional.ofNullable(occInspectedSpaceDTO)
                  .map(OccInspectedSpaceDTO::getOccInspectedSpaceElementDTOList)
                  .ifPresent(list -> list.forEach(occInspectedSpaceElementDTO -> {
                    OccInspectedSpaceElement occInspectedSpaceElement = modelMapper.map(occInspectedSpaceElementDTO, OccInspectedSpaceElement.class);
                    Integer inspectedSpaceElementId = updateInspectedSpaceElement(occInspectedSpaceElement, finalInspectionId, inspectedSpaceId);
                    occInspectedSpaceElementDTO.setInspectedSpaceElementId(inspectedSpaceElementId);
                  }));
        });
      }
    } catch (Exception e) {
      logger.error(String.format("Date: %s, %s", OffsetDateTime.now(), e));
      synchronizationLog.append("UploadInspectionFailException: ").append(e.getMessage());
      updateSynchronizationNotes(occInspectionDispatchDTO.getDispatchId(), synchronizationLog.toString());
      throw new UploadInspectionFailException("UploadInspectionFailException");
    }
    return occInspectionDispatchDTO;
  }

  private void validateInspectionExists(int inspectionId) {
    if (!occInspectionMapper.checkInspectionExists(inspectionId)) {
      throw new ResourceNotFoundException("Inspection", "id", inspectionId);
    }
  }

  private void validateInspectedElementExists(int inspectedElementId) {
    if (!occInspectedSpaceElementMapper.checkOccInspectedSpaceElementExists(inspectedElementId)) {
      throw new ResourceNotFoundException("Inspected Element", "id", inspectedElementId);
    }
  }

  private void validateInspectionAndElementAssociation(int inspectionId, int inspectedSpaceElementId) {
    if (!occInspectedSpaceElementMapper.checkInspectionAndElementAssociation(inspectionId, inspectedSpaceElementId)) {
      throw new ApiException(HttpStatus.BAD_REQUEST, "Inspected Element does not belongs to that inspection id");
    }
  }

  private void validateInspectionAndInspectedSpaceAssociation(int inspectionId, int inspectedSpaceId) {
    if (!occInspectedSpaceMapper.checkInspectionAndInspectedSpaceAssociation(inspectionId, inspectedSpaceId)) {
      throw new ApiException(HttpStatus.BAD_REQUEST, "Inspected Space does not belongs to that inspection id");
    }
  }

  private PhotoDoc createPhotoDoc(int bytesId, BlobDTO blobDTO, UserDTO user) {
    PhotoDoc photoDoc = new PhotoDoc();
    photoDoc.setBlobBytesId(bytesId);
    photoDoc.setPhotoDocDescription("photo for inspection id " + blobDTO.getInspectionId());
    photoDoc.setMunicode(user.getMunicode());
    photoDoc.setTitle(blobDTO.getBlobBytes().getFileName());
    photoDoc.setCreatedTS(blobDTO.getBlobBytes().getCreatedTS());
    photoDoc.setCreatedByUserid(blobDTO.getBlobBytes().getUploadedByUserId());
    photoDoc.setLastUpdatedTS(blobDTO.getBlobBytes().getCreatedTS());
    photoDoc.setLastUpdatedByUserId(blobDTO.getBlobBytes().getUploadedByUserId());
    photoDoc.setDateOfRecord(blobDTO.getBlobBytes().getCreatedTS());
    Integer typeId = blobTypeMapper.getBlobTypeId(photoDoc.getTitle());
    if (Objects.nonNull(typeId)) {
      photoDoc.setBlobTypeId(typeId);
    }
    return photoDoc;
  }

  private OccInspection createNewInspection(OccInspection occInspection, String propertyAddress, String unitNumber, UserDTO userDTO) {
    int userId = userDTO.getUserid();
    occInspection.setCreatedByUserId(userId);
    occInspection.setLastUpdatedByUserId(userId);
    occInspection.setOccPeriodId(municipalityMapper.getOccperiodByMunicode(userDTO.getMunicode()));
    occInspectionMapper.insertInspection(occInspection, propertyAddress, unitNumber);
    return occInspection;
  }

  private OccInspectionDispatch createNewOccInspectionDispatch(OccInspectionDispatch occInspectionDispatch, int userId, int inspectionId) {
    occInspectionDispatch.setInspectionId(inspectionId);
    occInspectionDispatchMapper.insertInspectionDispatch(occInspectionDispatch, userId);
    return occInspectionDispatch;
  }

  private void updateSynchronizationNotes(int dispatchId, String logs) {
    if (occInspectionDispatchMapper.checkDispatchInspectionExists(dispatchId)) {
      occInspectionDispatchMapper.updateSyncNotes(dispatchId, logs);
    }
  }

  private int updateInspectedSpaces(OccInspectedSpace occInspectedSpace, int inspectionId) {
    Integer inspectedSpaceId = occInspectedSpace.getInspectedSpaceId();
    Optional<OccInspectedSpace> existingSpace = Optional.ofNullable(occInspectedSpaceMapper.selectByOccInspectedSpaceId(inspectedSpaceId));

    if (existingSpace.isPresent()) {
      validateInspectionAndInspectedSpaceAssociation(inspectionId, inspectedSpaceId);
      occInspectedSpaceMapper.updateInspectedSpace(occInspectedSpace);
    } else {
      occInspectedSpace.setOccInspectionId(inspectionId);
      occInspectedSpaceMapper.insertInspectedSpace(occInspectedSpace);
      inspectedSpaceId = occInspectedSpace.getInspectedSpaceId();
    }

    return inspectedSpaceId;
  }

  private int updateInspectedSpaceElement(OccInspectedSpaceElement occInspectedSpaceElement, int inspectionId, int inspectedSpaceId) {
    Integer inspectedSpaceElementId = occInspectedSpaceElement.getInspectedSpaceElementId();
    Optional<OccInspectedSpaceElement> existingElement = Optional.ofNullable(occInspectedSpaceElementMapper.selectByOccInspectedSpaceElementId(inspectedSpaceElementId));
    if (existingElement.isPresent()) {
      validateInspectionAndElementAssociation(inspectionId, occInspectedSpaceElement.getInspectedSpaceElementId());
      occInspectedSpaceElementMapper.updateOccInspectedSpaceElement(occInspectedSpaceElement);
    } else {
      occInspectedSpaceElement.setInspectedSpaceId(inspectedSpaceId);
      occInspectedSpaceElementMapper.insertInspectedSpaceElement(occInspectedSpaceElement);
      inspectedSpaceElementId = occInspectedSpaceElement.getInspectedSpaceElementId();
    }
    return inspectedSpaceElementId;
  }
}
