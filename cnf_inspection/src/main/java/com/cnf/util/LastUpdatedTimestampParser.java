package com.cnf.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

public class LastUpdatedTimestampParser {

    public static Timestamp parseLastUpdatedToTimestamp(String timestampString) {
        if (Objects.nonNull(timestampString)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date parsedDate = null;
            try {
                parsedDate = dateFormat.parse(timestampString);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            return new Timestamp(parsedDate.getTime());
        }
        return null;
    }
}
