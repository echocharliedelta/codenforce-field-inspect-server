package com.cnf.util;

public class RedisConstants {

  public static final String LOGIN_USER_KEY = "login:user:token:";
  public static final Long LOGIN_USER_TTL = 1800L;

  public static final String LOGIN_MUNI_KEY = "login:user:muni:token:";
  public static final Long LOGIN_MUNI_TTL = 36000L;
}
